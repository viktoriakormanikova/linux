#!/bin/bash

ram_total=$( free | grep Mem | awk {'print $2'} )
ram_used=$( free | grep Mem | awk {'print $3'} )
ram_usage=$(($ram_used * 100 / $ram_total))

ram_alert=80

if [ $ram_usage -ge $ram_alert ]

then 

echo "RAM usage is above $ram_alert%: $ram_usage%" >> /root/ram.txt ; date >> /root/ram.txt

else 

echo "RAM usage is normal: $ram_usage% " >> /root/ram.txt ; date >> /root/ram.txt

fi 
