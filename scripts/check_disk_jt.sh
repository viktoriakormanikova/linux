#!/bin/bash

disk=/dev/sda1
# save current usage
current_usage=$( df -h | grep $disk  | awk {'print $5'} )
# remove %
current_usage=${current_usage%"%"}

max_usage=80
mydate=$(date)

if [ $current_usage -ge $max_usage ]

then 

echo "$mydate: Disk $disk critical space"

else 

echo "$mydate: Disk $disk has enough space" 

fi 
