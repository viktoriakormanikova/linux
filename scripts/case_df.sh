#!/bin/bash

if [ -d /mnt/backup/ ]; then echo "backup folder exists" 
else mkdir /mnt/backup/
fi 

echo -n "Which log do you want to backup? "
echo "secure, messages, dmesg can be used" 
read logs

case $logs in

 secure)
cp /var/log/secure /mnt/backup/ ;;

 messages)
cp /var/log/messages /mnt/backup/ ;; 

 dmesg)
cp /var/log/dmesg /mnt/backup/ ;;

 *)
echo "wrong log" ;;

esac 
 

