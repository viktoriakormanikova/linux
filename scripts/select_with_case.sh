#!/bin/bash

if [ -d "/home/developer/removable" ]
then
    echo "Are you sure you want to delete this folfer?"
    select yn in 1 2;
 do
        case $yn in
            1 )
                echo "Removing the folder...";
                rmdir /home/developer/removable;
                break;;
            2 )
                echo "Stopping the deletion.";
                exit 1;;
        esac

done

else
        echo "Folder does not exist"
        mkdir /home/developer/removable
    exit 1
