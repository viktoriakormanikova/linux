#!/bin/bash

echo "Enter one from 'secure', 'messages' or 'dmesg' to backup"
read log

if ! [ -d /mnt/backup ]
then
mkdir /mnt/backup
echo "/mnt/backup created"
fi

case $log in

secure)
cp /var/log/secure /mnt/backup ;;

messages)
cp /var/log/messages /mnt/backup ;;

dmesg)
cp /var/log/dmesg /mnt/backup ;;

*)
echo "Not valid option" ;;

esac
