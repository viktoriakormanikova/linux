#!/bin/bash


# sucasne vyuzitie
current_usage=$( df -h | grep '/dev/mapper/centos-root' | awk {'print $5'} )

# maximalne vyuzitie
max_usage=80%

if [ ${current_usage%?} -ge ${max_usage%?} ]

then 

echo "disk /root is full" >> disk_monitor_df.txt ; date >> disk_monitor_df.txt

else 

echo "all good  disk /root have space" >> disk_monitor_df.txt ; date >> disk_monitor_df.txt

fi 
 


