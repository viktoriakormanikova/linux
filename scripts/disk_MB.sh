#!/bin/bash

current_usage=$(df -h | grep '/dev/mapper/centos-root' | awk {'print$5'})
max_usage=80%

if [ ${current_usage%?} -ge ${max_usage%?} ]

then
	echo "Disk is full" >> /root/disk.txt; date >> /root/disk.txt
else
	echo "Disk is OK" >> /root/disk.txt; date >> /root/disk.txt
fi
