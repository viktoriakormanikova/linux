#!/bin/bash

echo "Which file you want to backup?"
echo "/var/log/ messages, secure, cron?"
read file

if ! [[ -d /mnt/backup ]]
then
mkdir /mnt/backup
echo "Dir /mnt/backup created"
fi

case $file in

  messages)

cp /var/log/messages /mnt/backup ;;

 secure)
	  
cp /var/log/secure /mnt/backup ;;
		
 cron)

cp /var/log/cron /mnt/backup ;;

 *)

echo "Not permited"  ;;


esac

