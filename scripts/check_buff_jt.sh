#!/bin/bash

# save current usage
current=$( free -h | grep "Mem"  | awk {'print $6'} )
# remove %
current=${current%"M"}

max_usage=500
mydate=$(date)

if [ $current -ge $max_usage ]

then 

echo "$mydate: Buff critical space usage"

else 

echo "$mydate: Buff has enough space on disk"

fi 
