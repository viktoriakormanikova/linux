#!/bin/bash

#you have new server

#we need:

# 1) information about uptime
date && uptime

# 2) information about hostname
hostname -A

# 3) information about check ram, disk
free -m && lsblk

# 4) information about new packages? - and redirect to /updates_possible
yum update list > /updates_possible/21_1_2023.txt

