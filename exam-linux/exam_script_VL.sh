#!/bin/bash

# how long OS is running

echo "Uptime" >> /app_data_27_1/info.txt
uptime >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt

# version kernel

echo "Kernel version:" >> /app_data_27_1/info.txt
uname -r >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt

# version OS

echo "OS version" >> /app_data_27_1/info.txt
cat /etc/*release >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt

# check RAM and disk

echo "RAM usage:" >> /app_data_27_1/info.txt
free -h >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt

echo "Disk usage:" >> /app_data_27_1/info.txt
df -h >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt

# check status of Apache

echo "Apache status:" >> /app_data_27_1/info.txt
systemctl status httpd | grep -e "Active\|Loaded\|Main" >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt

# Apache index.html test

echo "Testing index.html:" >> /app_data_27_1/info.txt
curl localhost >> /app_data_27_1/info.txt
echo "=================" >> /app_data_27_1/info.txt
