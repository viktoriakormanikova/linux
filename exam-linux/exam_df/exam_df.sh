#!/bin/bash

uptime                  >> /app_data_27_1/infodf.txt
uname -a                >> /app_data_27_1/infodf.txt
cat /etc/os-release     >> /app_data_27_1/infodf.txt
free                    >> /app_data_27_1/infodf.txt
lsblk -p                >> /app_data_27_1/infodf.txt
