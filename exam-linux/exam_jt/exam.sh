#!/bin/bash

FILE=/app_data_27_1/info.txt

if [ ! -f "$FILE" ]
	then
	touch $FILE
fi


echo "OS is running for: $(uptime | awk '{print $3, $4}')" > $FILE
echo "" >> $FILE

echo "Kernel version: $(uname -r)" >> $FILE
echo "" >> $FILE

cat /etc/*release* | grep -i "^name" >> $FILE
echo "" >> $FILE

echo "RAM Info:" >> $FILE

free -h >> $FILE
echo "" >> $FILE

df -h >> $FILE

echo "For info check $FILE"

sudo yum install -y httpd
sudo systemctl enable --now httpd
echo "My name: Jozef Tarkanic, My email: jozef.tarkanic@t-system.com" > /var/www/index.html
STATUS=$(systemctl status httpd | grep "running")

if [ "$STATUS" = "running" ]; then
	echo httpd running >> $FILE
fi

echo "" >> $FILE
curl localhost:80 >> $FILE

echo "" >> $FILE
echo "httpd PID: $(pgrep http)" >> $FILE
