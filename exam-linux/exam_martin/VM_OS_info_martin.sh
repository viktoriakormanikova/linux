#!/bin/bash

echo "Server is running for " > /app_data_27_1/info.txt
uptime >> /app_data_27_1/info.txt
echo "" >> /app_data_27_1/info.txt
echo "kernel version is " >> /app_data_27_1/info.txt
uname -r >> /app_data_27_1/info.txt
echo "" >> /app_data_27_1/info.txt
echo "OS version is " >> /app_data_27_1/info.txt
cat /etc/os-release | grep PRETTY_NAME >> /app_data_27_1/info.txt
echo "" >> /app_data_27_1/info.txt
echo "RAM status is:" >> /app_data_27_1/info.txt
free -h >> /app_data_27_1/info.txt
echo "" >> /app_data_27_1/info.txt
echo "Disk status is:" >> /app_data_27_1/info.txt
df -h >> /app_data_27_1/info.txt
