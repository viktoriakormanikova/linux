#!/bin/bash

echo "uptime" > /app_data_27_1/info.txt
 uptime >> /app_data_27_1/info.txt

echo "version kernel" >> /app_data_27_1/info.txt
 uname -r >> /app_data_27_1/info.txt

echo "version OS" >> /app_data_27_1/info.txt
 cat /etc/os-release >> /app_data_27_1/info.txt

echo "Ram disk info" >> /app_data_27_1/info.txt
 free -m >> /app_data_27_1/info.txt

echo "Disk info" >> /app_data_27_1/info.txt
 df -h >> /app_data_27_1/info.txt
