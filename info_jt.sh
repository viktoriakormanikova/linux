#!/bin/bash

echo  "SYSTEM INFO"
echo ""

uptime
echo ""

hostname
echo ""

uname -r
echo ""

cat /etc/os-release
echo ""

df -h
echo ""

free -h
echo ""

yum list updates > /updates_possible/23_1_2023.txt
