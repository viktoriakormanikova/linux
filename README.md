# linux


## Schedule
17.1.2023 - 27.1.2023

### DAY1
- **Assesment test**
  - perform set of basic Linux administration tasks on a given machine

- **Linux shell**
  - accessing Linux SHELL
  - navigating shortcuts and commands
  - switching between users
  - escalate privileges
  - get help

- **Filesystem Hierarchy Standard (FHS)**
  - navigation within Linux filesystem
  - absolute vs. relative paths
  - create and manage soft and hard links

- **Manipulating files and directories**
  - creation and deletion of files and folders
  - tools for displaying and processing files
  - copy, move and rename of files and folders

- **Linux Permissions**
  - set ownership and access rights for files and folders
  - SUID, SGID ans sticky bit

- **Users and Groups in Linux**
  - managing local users
  - setting passwords and password aging for local Users
  - manage user privileges

- **VIM Survival Kit**
  - basics for mastering the VIM editor

LABS:
- create directory structure according template
- adjust permissons for files and folders
- create users and groups
- write an document using VIM

### DAY2
- **Software Management**
  - managing software packages (YUM & APT)

- **Service Management**
  - managing system services

- **Storage Management**
  - managing disks
  - Logical Volume Management (LVM)

- **Cron**
  - scheduling jobs via cron



LABS:
- create filesystems using LVM
- install, configure and manage Web server
- install, configure, test and troubleshoot the ssh server
- configure ssh server for ssh keys authentication

### DAY3
- **Networking**
  - configure Networking
  - tools for network testing and troubleshooting

- **SSH**
  - install & configure SSHD
  - manage SSH keys
  - testing SSH
  - copy over SSH

LABS:
- configure & test networking between 2 machines
- configure ssh server for ssh keys authentication

### DAY4
- **BASH Scripting**
  - Intro to BASH
  - Variables
  - Read inputs
  - if/else statemens
  - Loops
  - Case statements

- **Version Source control with GIT**
  - intro to GIT
  - basic operations with git

LABS:
- create simple bash scripts from scratch:
  - script which displays info about server (hostname, distro, IPs, arch, kernel,..)
  - script which turns vm into go lang developer's machine (go lang installation)
  - script form managing local users (add/remove users)
- interconnect writing of scripts with git

### DAY5
- **Automation in practice**
  - developing & testing BASH scripts
  - automation via Ansible
    - intro to Ansible

LABS:
- developing scripts with help of git, gitlab CICD
- developing & testing Ansible playbooks

### DAY6
- **Docker**
  - intro to Docker
  - build Docker image using Dockerfile
  - start/stop/remove Docker containers
  - get to know dockerhub.com for publishing Images

LABS:
- build own Docker image 
- store and manage files related to build procedure in gitlab
- push Docker Image into docker registry

### DAY7
- **Welcome to Cloud world**
  - intro to Cloud
  - gettting familiar with the AWS cloud provider
  - deploy resources within AWS
  - focus on deploying EC2 machines

LABS:
- create networking stack within AWS
- deploy EC2 with running webserver
- deploy EC2 with k3s installed

### DAY8
- **Automate deployments via Terraform**
  - intro to Terraform
  - demo how deploy resources in AWS using Terraform

LABS:
- install terrafom
- review terraform docu
- deploy resources using terraform

### DAY9
- **Let's automate CI/CD**
  - intro to GitLab CICD / gitlab.com
  - demo how to manage directories/projects within gitlab
  - building CICD pipelines using Gitlab CICD

LABS:
- create an account in gitlab.com
- create directory/projects structure in gitlab
- build own CICD pipeline

### DAY10
- **Final Exam**
- Visit Server room
